function get_info() {
    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.5) {
            let tmp_id = Math.floor(Math.random() * 14000 + 6000);
            setTimeout(() => {
                resolve(tmp_id);
            }, timer);
        } else {
            setTimeout(() => {
                reject('get_info Failed');
            }, timer);
        }
    });
}

function get_firstname() {
    first_name_list = ['Adam', 'Eric', 'Peter'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list

    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {        
            let fn = first_name_list[Math.floor(Math.random() * 3)];
            setTimeout(() => {
                console.log("resolve get_firstname");
                resolve(fn);
            }, timer);
        } else {
            setTimeout(() => {
                console.log("reject get_firstname");
                reject('get_firstname Failed');
            }, timer);
        }
    });



}

function get_lastname() {
    last_name_list = ['Jones', 'Smith', 'Johnson'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list

    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {        
            let ln = last_name_list[Math.floor(Math.random() * 3)];
            setTimeout(() => {
                console.log("resolve get_lastname");
                resolve(ln);
            }, timer);
        } else {
            setTimeout(() => {
                console.log("reject get_lastname");
                reject('get_lastname Failed');
            }, timer);
        }
    });


}

function get_username() {
    username_list = ['Toyz', 'Faker', 'Dinter'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list

    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {        
            let un = username_list[Math.floor(Math.random() * 3)];
            setTimeout(() => {
                console.log("resolve get_username");
                resolve(un);
            }, timer);
        } else {
            setTimeout(() => {
                console.log("reject gey_username");
                reject('get_username Failed');
            }, timer);
        }
    });


}

function get_email() {
    email_list = ['asdf@google.com', 'qwer@microsoft.com', 'zxcv@cs.nthu.edu.tw'];
    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list

    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {        
            let el = email_list[Math.floor(Math.random() * 3)];
            setTimeout(() => {
                console.log("resolve get_email");
                resolve(el);
            }, timer);
        } else {
            setTimeout(() => {
                console.log("reject get_email");
                reject('get_email Failed');
            }, timer);
        }
    });



}

function get_address() {
    address_list = ['1027 Alpha Avenue', '3132 Kidd Avenue', '876 Jefferson Street'];

    // TODO : generate a success rate

    // TODO : generate a timer

    // TODO : random select a item from list

    return new Promise((resolve, reject) => {
        let success_rate = Math.random();
        let timer = Math.floor(Math.random() * 1000 + 500);
        if (success_rate > 0.1) {        
            let al = address_list[Math.floor(Math.random() * 3)];
            setTimeout(() => {
                console.log("resolve get_address");
                resolve(al);
            }, timer);
        } else {
            setTimeout(() => {
                console.log("reject get_address");
                reject('get_address Failed');
            }, timer);
        }
    });
}

function initApp() {
    var reSamplebtn = document.getElementById('resamplebtn');
    reSamplebtn.addEventListener('click', retrieve_data);
}

async function retrieve_data() {
    var txtInfoName = document.getElementById('user-info-name');
    var txtFirstName = document.getElementById('firstName');
    var txtLastName = document.getElementById('lastName');
    var txtUserName = document.getElementById('username');
    var txtEmail = document.getElementById('email');
    var txtAddress = document.getElementById('address');
    var boxReSample = document.getElementById('re-sample');
    txtInfoName.innerText = '-';
    txtFirstName.value = '-';
    txtLastName.value = '-';
    txtUserName.value = '-';
    txtEmail.value = '-';
    txtAddress.value = '-';
    try {
        // TODO : get_info first
        // TODO : call other function to get other data

        console.log('George Try');
        p0 = await get_info();
        txtInfoName.innerText = p0 + " 's Information";

        Promise.all([get_firstname(), get_lastname(), get_username(), get_email(), get_address()]).then(val => { 
            console.log(val);

            txtFirstName.value = val[0];
            txtLastName.value  = val[1];
            txtUserName.value  = val[2];
            txtEmail.value     = val[3];
            txtAddress.value   = val[4];


        }).catch(promise_error => {
            console.log("promise_error");
            txtInfoName.innerText = "failed";
            if(boxReSample.checked == true) retrieve_data();
        });

    } catch (e) {

        console.log("George catch error");
        txtInfoName.innerText = "failed";
        if(boxReSample.checked == true) retrieve_data();
    }
}

window.onload = function() {
    initApp();
}